module CloudStorage
  class S3Manager
    def initialize
      resource = Aws::S3::Resource.new(region: ENV.fetch('AWS_REGION', 'us-west-2'))
      @bucket = resource.bucket(ENV.fetch('AWS_BUCKET_NAME', 'daplagel-uploads'))
    end

    def upload(data, file_name)
      object = @bucket.object(file_name)
      object.put(
        {
          content_type: 'application/octet-stream',
          acl: 'public-read',
          body: data
        }
      )
      object.public_url
    end
  end
end