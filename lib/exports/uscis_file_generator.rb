module Exports
  class UscisFileGenerator
    def initialize(applications)
      @applications = applications
    end

    def build
      output = [header_row]
      @applications.sort_by(&:first_name).each do |application|
        output << generate_application_row(application)
      end
      output.join("\n")
    end

    private

    def header_row
      ['Full Name', 'Visa Type', 'Application Date', 'BatchId', 'Guarantor Code'].join(',')
    end

    def generate_application_row(application)
      [
        "#{application.first_name} #{application.last_name}",
        formatted_visa_type(application.visa_type),
        current_date,
        application.uuid,
        guarantor_code
      ].join(',')
    end

    def formatted_visa_type(visa_type)
      {
        VisaApplication::O1_TYPE => 'O-1',
        VisaApplication::B1_TYPE => 'B-1',
        VisaApplication::H1B_TYPE => 'H-1B'
      }[visa_type]
    end

    def current_date
      @current_date ||= Time.zone.now.strftime('%F')
    end

    def guarantor_code
      'DAPLAGEL'
    end
  end
end