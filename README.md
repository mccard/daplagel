# Daplagel

## Overview
This is a mock application for a coding assignment. Please see [LEGALPAD_CHALLENGE.md](LEGALPAD_CHALLENGE.md) for 
instructions on how to complete the assignment

## Technologies used
* Ruby 2.6.6
* Rails 6.1.3
* React
* Sidekiq
* S3

## Setup
1. Clone repo: `git clone git@github.com:beansupreme/daplagel.git`
1. Bundle install `bundle`
1. Yarn install `yarn`
1. Setup db `rake db:setup && rake db:migrate && rake db:seed`
1. Run rails server `rails s`

### Optional setup (not needed for this assignment)
1. If you wish to run background jobs, setup redis and run sidekiq: `bundle exec sidekiq`
1. If you wish to test the aws integration, set these ENV vars: 
```bash
export AWS_BUCKET_NAME="bucket-name"
export AWS_REGION="us-east-2"
export AWS_ACCESS_KEY="AWS_ACCESS_KEY"
export AWS_SECRET="AWS_SECRET_KEY"
````



## Features

1. Visa Info Bar
    * This gives a basic overview of the visa applications in the system, and what state they are in    
1. Visa Applications Table
    * When expanded, shows all of the visa applications and their current state
1. Submit Draft Applications
    * When clicked, this will fire off a chain of events:
        1. A sidekiq worker will be scheduled, and the request will return 200
        2. When the worker starts, it will build a USCIS formatted csv file with the data from the 
        applications in a 'draft' state.
        3. The worker will then upload this file to S3
        4. After the file is in S3, it will create a new `VisaExport` record
        5. Sets all current `draft` `VisaApplication` records to the `pending` state        
1. Visa Exports Table
    * This allows the user to view all of the previous exports, and download the files   
    

