## Legalpad Ruby on Rails Coding Case Study


### Instructions
Using this repository as a base, we would like you to add a new feature to this application. Treat this
feature as you would in a real-life work scenario.

You can either fork this repo, or create a new feature branch. Please do not push to `master` directly. 

Once you are happy with your work, please submit a pull request into `master` with your requested feature changes. 

This application runs on heroku. If you'd like, you can try deploying the application on heroku as you work, but 
getting it running smoothly on heroku is not a requirement of the assignment. 

Your fork/branch should be able to run locally however. If any new instructions are needed to run the application,
please put them in the pull request description, or update the [README](README.md)


### Feature Description 

#### Requirement 1
Currently a visa application record holds the person's first name and last name. The current structure prevents us from 
one person having multiple visa applications.  

A person should be able to have multiple visa applications.

Restructure the database tables so that a person can have multiple visas (e.g. Jane Doe can have an H1-B AND an O-1). The data 
does not need to be migrated over to the new db structure for this exercise.  

#### Requirement 2
Currently there is no way to create a new person or visa application. Extend the application to allow for the creation of
new visa applications and people records.

You can choose to write the front end code in React or server rendered erb templates.

Include a link or button to this new feature on the home page. 
This application data must be persisted to the database. 


### Things we would like to see
- The requested feature completed as described
- Thorough automated testing
- Well thought-out database schema
- Clean modular code
- Documentation in your pull request describing how the feature works
