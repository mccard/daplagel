class VisaApplicationsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @visa_applications = VisaApplication.all
  end

  def info
    visa_application_info = {
      total_applications: VisaApplication.count,
      approved_applications: VisaApplication.approved.count,
      pending_applications: VisaApplication.pending.count,
      rejected_applications: VisaApplication.rejected.count,
      draft_applications: VisaApplication.draft.count
    }
    render json: visa_application_info
  end

  def submit_drafts
    VisaApplicationProcessor.perform_async
    render json: {}
  end
end