class VisaApplication < ApplicationRecord
  STATUSES = [
    PENDING_STATUS = 'pending'.freeze,
    REJECTED_STATUS = 'rejected'.freeze,
    APPROVED_STATUS = 'approved'.freeze,
    DRAFT_STATUS = 'draft'.freeze,
  ].freeze

  VISA_TYPES = [
    O1_TYPE = 'o-1'.freeze,
    H1B_TYPE = 'h-1-b'.freeze,
    B1_TYPE = 'b-1'.freeze,
  ].freeze

  validates_presence_of :first_name, :last_name
  validates :visa_type, inclusion: VISA_TYPES, presence: true
  validates :status, inclusion: STATUSES, presence: true

  before_save :set_uuid

  scope :approved, -> { where(status: APPROVED_STATUS) }
  scope :pending, -> { where(status: PENDING_STATUS) }
  scope :rejected, -> { where(status: REJECTED_STATUS) }
  scope :draft, -> { where(status: DRAFT_STATUS) }

  private

  def set_uuid
    self.uuid = SecureRandom.uuid unless self.uuid.present?
  end
end
