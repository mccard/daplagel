class VisaApplicationProcessor
  include Sidekiq::Worker

  def perform
    draft_applications = VisaApplication.draft
    return unless draft_applications.present?
    file_data = Exports::UscisFileGenerator.new(draft_applications).build
    s3_url = CloudStorage::S3Manager.new.upload(file_data, filename)
    draft_applications.update_all(status: VisaApplication::PENDING_STATUS)
    VisaExport.create(export_url: s3_url)
  rescue StandardError => e
    # Send websocket notification here
    raise e
  end

  private

  def filename
    "visa_applications_#{Time.zone.now.strftime('%Y%m%dT%H%M%S')}.csv"
  end
end