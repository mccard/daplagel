// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { VisaApplicationsTable } from "../visa_applications/VisaApplicationsTable"
import { VisaInfoBar } from "../visa_applications/VisaInfoBar"
import { VisaApplicationsContainer } from '../visa_applications/VisaApplicationsContainer'
import { VisaExportsContainer } from '../visa_applications/VisaExportsContainer'

const App = () => (
  <div id="app" className="container px-10 mx-auto">
    <VisaInfoBar />
    <VisaApplicationsContainer />
    <VisaExportsContainer />
  </div>
  
)

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <App />,
    document.body.appendChild(document.createElement('div')),
  )
})
