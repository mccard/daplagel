import React, { useState } from "react"
import { VisaApplicationsTable } from "../visa_applications/VisaApplicationsTable"
import axios from "axios";


export function VisaApplicationsContainer(props) {

  const [showTable, setShowTable] = useState(false);
  const [requestRunning, setRequestRunning] = useState(false);
  const [requestFailed, setRequestFailed] = useState(false);

  const submitDraftApplications = () => {
    setRequestRunning(true);
    axios
      .post('/visa_applications/submit_drafts')
      .then(response => {
        setRequestRunning(false);
      })
      .catch(error => {
        console.log(error)
        setRequestFailed(false);
        setRequestRunning(false);
      })
  }

  return (
    <div id="visa-table-container">

      {showTable && <VisaApplicationsTable />}

      <div className="text-center">
        <button onClick={() => setShowTable(!showTable)} className="text-lg text-center text-white font-semibold rounded-full px-4 py-2 leading-normal border border-purple bg-indigo-500 hover:bg-indigo-600 ">
          {`${showTable ? "Hide Visa Applications" : "Show Visa Applications"}`}
        </button>

        <button onClick={() => submitDraftApplications()} className={`text-lg text-center text-white font-semibold rounded-full px-4 py-2 leading-normal border border-purple bg-pink-500 hover:bg-pink-600 ${requestRunning ? "cursor-not-allowed" : ""}`}>
          {`${requestRunning ? "Submitting..." : "Submit Draft Applications"}`}
        </button>
      </div>
  </div>
  )
}

VisaApplicationsContainer.propTypes = {};