import React, { useEffect, useState } from "react"
import axios from "axios"

export function VisaApplicationsTable(props) {

  const [loading, setLoading] = useState(false);
  const [requestFailed, setRequestFailed] = useState(true);
  const [visaApplications, setVisaApplications] = useState([]);

  useEffect(() => {
    const fetchVisaApplications = () => {
      setLoading(true)
      axios
        .get('/visa_applications/')
        .then(response => {
          setRequestFailed(false);
          setVisaApplications(response.data);
        })
        .catch(error => {
          console.log(error)
          setRequestFailed(true);
        })
        .finally(() => setLoading(false))
    }
    fetchVisaApplications();
  }, [])

  if (loading) {
    return <p>Retrieving latest visa applications...</p>
  }

  if (requestFailed) {
    return <p>Request failed.</p>
  }

  return (
    <div className="visa-table">
      <div className="bg-white rounded overflow-scroll md:overflow-hidden border-t border-l border-r border-b border-gray-400 p-4">
        <table className="table-auto md:w-full">
          <caption className="text-xl">Visa Applications</caption>
          <thead>
            <tr>
              <th className="px-4 py-2">First Name</th>
              <th className="px-4 py-2">Last Name</th>
              <th className="px-4 py-2">Type</th>
              <th className="px-4 py-2">Status</th>
            </tr>
          </thead>
          <tbody>
            {visaApplications.map(visa => (
              <tr key={visa.id}>
              <td className="border px-4 py-2">{visa.first_name}</td>
              <td className="border px-4 py-2">{visa.last_name}</td>
              <td className="border px-4 py-2">{visa.visa_type}</td>
              <td className="border px-4 py-2">{visa.status}</td>
            </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  )
}