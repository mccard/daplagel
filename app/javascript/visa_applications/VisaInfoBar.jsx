import React, { useState, useEffect } from "react"
import axios from 'axios';

export function VisaInfoBar(props) {

  const [loading, setLoading] = useState(false);
  const [requestFailed, setRequestFailed] = useState(false);
  const [visaInfo, setVisaInfo] = useState({
          draft_applications: 0,
          approved_applications: 0,
          pending_applications: 0,
          rejected_applications: 0,
          total_applications: 0
        })

  useEffect(() => {
    const fetchVisaInfo = () => {
      setLoading(true);
      axios
        .get('/visa_applications/info')
        .then(response => {
          setRequestFailed(false);
          setVisaInfo(response.data)
        })
        .catch(error => {
          console.log(error)
          setRequestFailed(true);
        })
        .finally(() => setLoading(false))
    }
    fetchVisaInfo();
  }, [])

  if (loading) {
    return <p>Retrieving latest visa applications data...</p>
  }

  if (requestFailed) {
    return <p>We're sorry, something went wrong. Please hunt down the developer.</p>
  }

  return (
    <div className="info-bar">
      <section className="bg-white mx-auto overflow-hidden text-center py-10">
        <p className="text-xl py-2">
          There are {visaInfo.total_applications} total Visa Applications.
        </p>
        <ul>
          <li className="inline-block px-4 py-2 text-sm border rounded-full bg-blue-500">Draft Applications: {visaInfo.draft_applications}</li>
          <li className="inline-block px-4 py-2 text-sm border rounded-full bg-green-500">Approved Applications: {visaInfo.approved_applications}</li>
          <li className="inline-block px-4 py-2 text-sm border rounded-full bg-yellow-500">Pending Applications: {visaInfo.pending_applications}</li>
          <li className="inline-block px-4 py-2 text-sm border rounded-full bg-red-500">Rejected Applications: {visaInfo.rejected_applications}</li>
        </ul>
      </section>
    </div>
  )
}