import React, { useState } from "react"
import { VisaExportTable } from "../visa_applications/VisaExportTable"

export function VisaExportsContainer() {
  const [showTable, setShowTable] = useState(false);

  return (
    <div id="visa-exports-container">
      <div>
        {showTable && <VisaExportTable />}

        <div className="text-center">
          <button onClick={() => setShowTable(!showTable)} className="text-lg text-center text-white font-semibold rounded-full px-4 py-2 leading-normal border border-purple bg-indigo-500 hover:bg-indigo-600 ">
            {`${showTable ? "Hide Visa Exports" : "Show Visa Exports"}`}
          </button>
        </div>
      </div>
    </div>
  )
}