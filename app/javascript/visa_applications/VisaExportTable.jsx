import React, { useEffect, useState } from "react"
import axios from "axios"

export function VisaExportTable() {

  const [loading, setLoading] = useState(false);
  const [requestFailed, setRequestFailed] = useState(false);
  const [visaExports, setVisaExports] = useState([]);

  useEffect(() => {
    const fetchVisaExports = () => {
      setLoading(true);
      axios
        .get('/visa_exports/')
        .then(response => {
          setRequestFailed(false);
          setVisaExports(response.data);
        })
        .catch(error => {
          console.log(error)
          setRequestFailed(true);
        })
        .finally(() => setLoading(false))
    }
    fetchVisaExports();
  }, [])

  if (loading) {
    return <p>Retrieving latest visa exports...</p>
  }

  if (requestFailed) {
    return <p>Failed to retrieve visa exports</p>
  }

  return (
    <div className="visa-export-table my-8">
      <div className="bg-white rounded overflow-scroll md:overflow-hidden border-t border-l border-r border-b border-gray-400 p-4 ">
        <table className="table-auto md:w-full">
          <caption className="text-xl">Visa Exports</caption>
          <thead>
            <tr>
              <th className="px-4 py-2">Time</th>
              <th className="px-4 py-2">File</th>
            </tr>
          </thead>
          <tbody>
            {visaExports.map(visaExport => (
              <tr key={visaExport.id}>
                <td className="border px-4 py-2">{visaExport.created_at}</td>
                <td className="border px-4 py-2">
                  <a href={visaExport.export_url}>Download</a>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  )
}

VisaExportTable.propTypes = {};