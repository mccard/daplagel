require 'rails_helper'

RSpec.describe VisaApplication, type: :model do
  describe 'validations' do
    it 'requires the presence of first and last name' do
      new_app = VisaApplication.new(visa_type: VisaApplication::H1B_TYPE)

      expect(new_app.valid?).to eq(false)
      expect(new_app.errors).to match_array(["First name can't be blank", "Last name can't be blank"])
      new_app.first_name = 'Bill'
      new_app.last_name = 'Williamson'
      new_app.save

      expect(new_app.valid?).to eq(true)
    end

    it 'validates that visa_type is either o-1, h-1-b, or b-1' do
      new_app = VisaApplication.new(first_name: 'Jim', last_name: 'Bo', visa_type: '')
      expect(new_app.valid?).to eq(false)

      expect(new_app.errors).to match_array(["Visa type can't be blank", "Visa type is not included in the list"])

      new_app.visa_type = 'A-100-C'
      expect(new_app.valid?).to eq(false)
      expect(new_app.errors).to match_array(["Visa type is not included in the list"])

      new_app.visa_type = 'h-1-b'
      new_app.save
      expect(new_app.valid?).to eq(true)
    end

    it 'validates that status is in the included list' do
      new_app = create(:visa_application)
      new_app.status = nil
      new_app.save

      expect(new_app.valid?).to eq(false)

      expect(new_app.errors).to match_array(["Status can't be blank", "Status is not included in the list"])

      new_app.status = 'purgatory'
      expect(new_app.valid?).to eq(false)
      expect(new_app.errors).to match_array(["Status is not included in the list"])

      new_app.status = 'approved'
      new_app.save
      expect(new_app.valid?).to eq(true)
    end
  end

  describe 'scopes' do
    let!(:pending_application) do
      create(:visa_application, :pending)
    end
    let!(:approved_application) do
      create(:visa_application, :approved)
    end
    let!(:rejected_application) do
      create(:visa_application, :rejected)
    end
    let!(:draft_application) do
      create(:visa_application, :draft)
    end

    describe 'pending' do
      it 'returns all pending applications' do
        expect(VisaApplication.pending).to match_array(pending_application)
      end
    end

    describe 'approved' do
      it 'returns all approved applications' do
        expect(VisaApplication.approved).to match_array(approved_application)
      end
    end

    describe 'rejected' do
      it 'returns all rejected applications' do
        expect(VisaApplication.rejected).to match_array(rejected_application)
      end
    end

    describe 'draft' do
      it 'returns all draft applications' do
        expect(VisaApplication.draft).to match_array(draft_application)
      end
    end

  end

  describe '#status' do
    it 'defaults to pending status' do
      new_app = VisaApplication.new(first_name: 'Jim', last_name: 'Bo', visa_type: 'h-1-b')
      expect(new_app.valid?).to eq(true)

      expect(new_app.status).to eq('pending')
    end
  end

  describe '#uuid' do
    it 'generates a UUID if none is provided' do
      new_application = create(:visa_application)
      expect(new_application.uuid).not_to be_nil
    end

    it 'uses the provided UUID if given' do
      new_application = create(:visa_application, uuid: 'MY_SPECIAL_ID')
      expect(new_application.uuid).to eq('MY_SPECIAL_ID')
    end
  end
end
