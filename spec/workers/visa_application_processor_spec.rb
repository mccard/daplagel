require 'rails_helper'

describe VisaApplicationProcessor do
  describe '#perform' do
    let!(:draft_application) do
      create(:visa_application, :draft, :b1, first_name: 'Young', last_name: 'Blood')
    end
    let(:mock_file_generator) { instance_double(Exports::UscisFileGenerator, build: '') }
    let(:mock_s3_manager) { instance_double(CloudStorage::S3Manager, upload: true) }

    before do
      allow(Exports::UscisFileGenerator).to receive(:new).and_return(mock_file_generator)
      travel_to(Time.zone.parse('2019-10-15 13:04:12'))
    end

    it 'builds an export file' do
      expect(Exports::UscisFileGenerator).to receive(:new)
                                               .with([draft_application])
                                               .and_return(mock_file_generator)
      expect(mock_file_generator).to receive(:build)

      described_class.perform_async
    end

    it 'streams the file to S3' do
      stub_data = "Header 1, Header 2/n Jim, Bo"

      allow(mock_file_generator).to receive(:build).and_return(stub_data)

      expect(CloudStorage::S3Manager).to receive(:new).and_return(mock_s3_manager)

      expect(mock_s3_manager).to receive(:upload).with(stub_data, 'visa_applications_20191015T130412.csv')

      described_class.perform_async
    end

    it 'creates a VisaExport record' do
      expect {
        described_class.perform_async
      }.to change(VisaExport, :count).by(1)

      last_export = VisaExport.last
      expect(last_export.export_url).to eq('https://daplegal-test.s3.us-east-2.amazonaws.com/visa_applications_20191015T130412.csv')
    end

    context 'with no draft applications' do
      it 'does not create a file or upload to s3' do
        VisaApplication.draft.destroy_all

        expect(Exports::UscisFileGenerator).not_to receive(:new)
        expect(CloudStorage::S3Manager).not_to receive(:new)

        described_class.perform_async
      end
    end

    context 'when the file generation is successful' do
      it 'sets all draft applications to "pending"' do
        expect(draft_application.status).to eq('draft')

        described_class.perform_async

        expect(draft_application.reload.status).to eq('pending')
      end
    end

    context 'when the file generation fails' do
      it 'leaves draft applications in the "draft" state' do
        allow(mock_file_generator).to receive(:build).and_raise('Eeek!')

        expect {
          described_class.perform_async
        }.to raise_error('Eeek!')

        expect(draft_application.reload.status).to eq('draft')
      end
    end
  end
end