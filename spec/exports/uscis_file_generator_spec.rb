require 'rails_helper'

describe Exports::UscisFileGenerator do
  describe '#build' do

    let!(:draft_application_tony) do
      create(:visa_application, :draft, :o1, first_name: 'Tony', last_name: 'Hawk', uuid: 'GREAT_SKATER')
    end
    let!(:draft_application_elanor) do
      create(:visa_application, :draft, :b1, first_name: 'Elanor', last_name: 'Rigby', uuid: 'BEATLES')
    end
    let!(:draft_application_wilson) do
      create(:visa_application, :draft, :h1b, first_name: 'Wilson', last_name: 'Stillhavefun', uuid: 'PHISH')
    end
    let(:draft_applications) { [draft_application_tony, draft_application_elanor, draft_application_wilson] }

    before(:each) do
      travel_to(Time.zone.parse('2019-10-13 12:15:30'))
    end

    subject { described_class.new(draft_applications) }


    it 'generates data with the expected format and content' do
      expected_content = <<~CSV
        Full Name,Visa Type,Application Date,BatchId,Guarantor Code
        Elanor Rigby,B-1,2019-10-13,BEATLES,DAPLAGEL
        Tony Hawk,O-1,2019-10-13,GREAT_SKATER,DAPLAGEL
        Wilson Stillhavefun,H-1B,2019-10-13,PHISH,DAPLAGEL
      CSV

      expect(subject.build).to eq(expected_content.chomp)
    end

  end
end