require 'rails_helper'

describe '/visa_applications' do
  let!(:approved_application) do
    create(:visa_application, :approved, :h1b, first_name: 'George', last_name: 'Jungle')
  end
  let!(:pending_application) do
    create(:visa_application, :pending, :o1, first_name: 'Mr', last_name: 'Invisible')
  end
  let!(:rejected_application) do
    create(:visa_application, :rejected, :b1, first_name: 'Dr', last_name: 'Evil')
  end
  let!(:draft_application) do
    create(:visa_application, :draft, :b1, first_name: 'Young', last_name: 'Blood')
  end

  before do
    travel_to(Time.zone.parse('2019-10-15 18:15:10'))
  end

  describe 'GET /visa_applications/info' do

    it 'provides some metadata about the existing visa applications in the system' do
      get '/visa_applications/info'

      expect(response).to have_http_status(:success)

      json_body = JSON.parse(response.body)
      expected_output = {
        'total_applications' => 4,
        'approved_applications' => 1,
        'pending_applications' => 1,
        'rejected_applications' => 1,
        'draft_applications' => 1
      }
      expect(json_body).to eq(expected_output)
    end
  end

  describe 'GET /visa_applications/' do
    it 'provides some metadata about the existing visa applications in the system' do
      get '/visa_applications/'

      expect(response).to have_http_status(:success)

      json_body = JSON.parse(response.body)
      expected_output = [
        {
          'id' => approved_application.id,
          'first_name' => 'George',
          'last_name' => 'Jungle',
          'visa_type' => 'h-1-b',
          'status' => 'approved',
        },
        {
          'id' => pending_application.id,
          'first_name' => 'Mr',
          'last_name' => 'Invisible',
          'visa_type' => 'o-1',
          'status' => 'pending',
        },
        {
          'id' => rejected_application.id,
          'first_name' => 'Dr',
          'last_name' => 'Evil',
          'visa_type' => 'b-1',
          'status' => 'rejected',
        },
        {
          'id' => draft_application.id,
          'first_name' => 'Young',
          'last_name' => 'Blood',
          'visa_type' => 'b-1',
          'status' => 'draft',
        }
      ]
      expect(json_body).to match_array(expected_output)
    end
  end

  describe 'POST /visa_applications/submit_drafts' do
    it 'returns a successful response' do
      post '/visa_applications/submit_drafts'

      expect(response).to have_http_status(:success)
    end

    it 'sets all draft applications to "pending"' do
      expect(draft_application.status).to eq('draft')

      post '/visa_applications/submit_drafts'

      expect(draft_application.reload.status).to eq('pending')
    end

    it 'creates a VisaExport record' do
      expect(VisaExport.count).to eq(0)

      post '/visa_applications/submit_drafts'

      expect(VisaExport.count).to eq(1)
      last_export = VisaExport.last
      expect(last_export.export_url).to eq("https://daplegal-test.s3.us-east-2.amazonaws.com/visa_applications_20191015T181510.csv")
    end
  end

end