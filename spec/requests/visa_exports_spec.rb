require 'rails_helper'

describe '/visa_exports' do
  let!(:first_export) do
    create(:visa_export, export_url: 'http://test.foo/file.csv')
  end
  let!(:second_export) do
    create(:visa_export, export_url: 'http://test.bar/another-file.csv')
  end

  describe 'GET /visa_exports/' do
    it 'returns the existing visa exports in JSON format' do
      get '/visa_exports/'

      expect(response).to have_http_status(:success)

      json_body = JSON.parse(response.body)
      expected_output = [
        {
          'id' => first_export.id,
          'export_url' => 'http://test.foo/file.csv',
          'created_at' => anything
        },
        {
          'id' => second_export.id,
          'export_url' => 'http://test.bar/another-file.csv',
          'created_at' => anything
        }
      ]
      expect(json_body).to match_array(expected_output)
    end
  end

end