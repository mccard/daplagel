require 'rails_helper'
require 'sidekiq/testing'

describe 'visiting the home page', type: :feature, js: true do

  before do
    Sidekiq::Testing.inline!
    travel_to(Time.zone.parse('2019-10-15 14:01:32'))
  end

  let!(:approved_application) do
    create(:visa_application, :approved, :o1, first_name: 'Fred', last_name: 'Armisen')
  end
  let!(:pending_application_one) do
    create(:visa_application, :pending, :h1b, first_name: 'Jane', last_name: 'Goodall')
  end
  let!(:pending_application_two) do
    create(:visa_application, :pending, :b1, first_name: 'Boe', last_name: 'Nobo')
  end
  let!(:rejected_application) do
    create(:visa_application, :rejected, :b1, first_name: 'Chimp', last_name: 'Anzee')
  end
  let!(:draft_application) do
    create(:visa_application, :draft, :o1, first_name: 'Baby', last_name: 'Face')
  end

  it 'allows the user to see applications' do
    visit '/'

    expect(page).to have_content('There are 5 total Visa Applications')
    expect(page).to have_content('Pending Applications: 2')
    expect(page).to have_content('Rejected Applications: 1')
    expect(page).to have_content('Approved Applications: 1')
    expect(page).to have_content('Draft Applications: 1')

    click_on 'Show Visa Applications'

    visa_table = find(:table, 'Visa Applications')

    expect(visa_table).to have_table_row('First Name' => 'Fred', 'Last Name' => 'Armisen', 'Status' => 'approved', 'Type' => 'o-1')
    expect(visa_table).to have_table_row('First Name' => 'Jane', 'Last Name' => 'Goodall', 'Status' => 'pending', 'Type' => 'h-1-b')
    expect(visa_table).to have_table_row('First Name' => 'Boe', 'Last Name' => 'Nobo', 'Status' => 'pending', 'Type' => 'b-1')
    expect(visa_table).to have_table_row('First Name' => 'Chimp', 'Last Name' => 'Anzee', 'Status' => 'rejected', 'Type' => 'b-1')
    expect(visa_table).to have_table_row('First Name' => 'Baby', 'Last Name' => 'Face', 'Status' => 'draft', 'Type' => 'o-1')

    click_on 'Hide Visa Applications'

    expect(page).not_to have_content 'First Name'

    click_on 'Show Visa Applications'

    click_on 'Submit Draft Applications'

    click_on 'Show Visa Exports'

    visa_export_table = find(:table, 'Visa Exports')

    expect(visa_export_table).to have_table_row('Time' => '2019-10-15T14:01:32.000Z', 'File' => 'Download')

    click_on 'Hide Visa Exports'

    click_on 'Hide Visa Applications'

    click_on 'Show Visa Applications'


    expect(visa_table).to have_table_row('First Name' => 'Baby', 'Last Name' => 'Face', 'Status' => 'pending', 'Type' => 'o-1')
  end
end