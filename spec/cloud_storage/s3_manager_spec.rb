require 'rails_helper'

describe CloudStorage::S3Manager do
  describe '#upload' do
    let(:stub_s3_object) { instance_double(Aws::S3::Object, put: '', public_url: 'http://custom.aws.wrapper/test-file.csv') }
    let(:stub_s3_bucket) { instance_double(Aws::S3::Bucket, object: stub_s3_object) }
    let(:stub_s3_resource) { instance_double(Aws::S3::Resource, bucket: stub_s3_bucket) }
    let(:file_data) { "Header1, Header2\n Row1, Row2" }

    before do
      ENV['AWS_REGION']='us-east-2'
      ENV['AWS_BUCKET_NAME']='daplegal-test'
      allow(Aws::S3::Resource).to receive(:new).and_return(stub_s3_resource)
    end

    it 'puts the file to S3' do
      expect(Aws::S3::Resource).to receive(:new).with(region: 'us-east-2').and_return(stub_s3_resource)

      expect(stub_s3_resource).to receive(:bucket).with('daplegal-test').and_return(stub_s3_bucket)
      expect(stub_s3_bucket).to receive(:object).with('test-file.csv').and_return(stub_s3_object)

      expected_options = {
        content_type: 'application/octet-stream',
        acl: 'public-read',
        body: file_data
      }

      expect(stub_s3_object).to receive(:put).with(expected_options)

      subject.upload(file_data, 'test-file.csv')
    end
    
    it 'returns the public url' do
      expect(subject.upload(file_data, 'test-file.csv')).to eq('http://custom.aws.wrapper/test-file.csv')
    end
  end
end