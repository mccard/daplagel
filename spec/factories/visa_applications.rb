FactoryBot.define do
  factory :visa_application do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    visa_type { VisaApplication::H1B_TYPE }

    trait :pending do
      status { VisaApplication::PENDING_STATUS }
    end

    trait :rejected do
      status { VisaApplication::REJECTED_STATUS }
    end

    trait :approved do
      status { VisaApplication::APPROVED_STATUS }
    end

    trait :draft do
      status { VisaApplication::DRAFT_STATUS }
    end

    trait :h1b do
      visa_type { VisaApplication::H1B_TYPE }
    end

    trait :b1 do
      visa_type { VisaApplication::B1_TYPE }
    end

    trait :o1 do
      visa_type { VisaApplication::O1_TYPE }
    end
  end
end