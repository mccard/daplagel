FactoryBot.define do
  factory :visa_export do
    export_url { Faker::Internet.url }
  end
end