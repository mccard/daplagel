require 'sidekiq/web'
Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'home#index'

  resources :visa_applications, only: [:index], defaults: {format: :json} do
    collection do
      get 'info'
      post 'submit_drafts'
    end
  end

  resources :visa_exports, only: [:index], defaults: {format: :json}
  mount Sidekiq::Web => '/admin/sidekiq'
end
