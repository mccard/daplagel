if ENV.fetch('AWS_ACCESS_KEY', '').present? && ENV.fetch('AWS_SECRET', '').present?
  Aws.config.update({
                      access_key_id: ENV['AWS_ACCESS_KEY'],
                      secret_access_key: ENV['AWS_SECRET']
                    })
else
  Rails.logger.info "No AWS credentials found, stubbing aws interactions"
  Aws.config.update(stub_responses: true)
end

