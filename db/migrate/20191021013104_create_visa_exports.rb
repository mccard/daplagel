class CreateVisaExports < ActiveRecord::Migration[6.0]
  def change
    create_table :visa_exports do |t|
      t.string :export_url

      t.timestamps
    end
  end
end
