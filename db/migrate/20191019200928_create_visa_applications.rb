class CreateVisaApplications < ActiveRecord::Migration[6.0]
  def change
    create_table :visa_applications do |t|
      t.string :first_name
      t.string :last_name
      t.string :origin_country
      t.string :visa_type
      t.string :status, default: 'pending'

      t.timestamps
    end
  end
end
