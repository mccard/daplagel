class AddUuidToVisaApplications < ActiveRecord::Migration[6.0]
  def change
    add_column :visa_applications, :uuid, :string
  end
end
