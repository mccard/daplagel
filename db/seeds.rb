# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
require 'factory_bot'
require 'faker'

3.times do
  FactoryBot.create(:visa_application, :draft)
  FactoryBot.create(:visa_application, :approved)
  FactoryBot.create(:visa_application, :pending)
  FactoryBot.create(:visa_application, :rejected)
end